package com.consult.painter.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

class PainterDAOTest {

	private static final String ID = "5db32d6f";
	private static final String NOMBRE= "kepa";
	private static final String APELLIDO= "Arrizabalaga";
	private static final String LUGARDENACIIENTO = "España";
	private static final String PINTURAMASFAMOSA= "No ha sido un arquero seguro";
	
	 PainterDAO dao;
	
	
	@Test
	void test() throws Exception {
		
		PainterDAO dao = new PainterDAO() ;
		dao.setApellido( "Arrizabalaga");
		dao.setId("5db32d6f");
		dao.setLugarDeNacimiento("España");
		dao.setNombre( "kepa");
		dao.setPinturaMasFamosa("No ha sido un arquero seguro");
        Assert.assertEquals("ID ok", ID, dao.getId());
        Assert.assertEquals("NOMBRE ok", NOMBRE, dao.getNombre());
        Assert.assertEquals("APELLIDO ok", APELLIDO, dao.getApellido());
        Assert.assertEquals("LUGARDENACIIENTO ok", LUGARDENACIIENTO, dao.getLugarDeNacimiento());
        Assert.assertEquals("PINTURAMASFAMOSA ok", PINTURAMASFAMOSA, dao.getPinturaMasFamosa());
	}
	
	@After
	void message() {
		System.out.println("La prueba salio bien");
	}

}
