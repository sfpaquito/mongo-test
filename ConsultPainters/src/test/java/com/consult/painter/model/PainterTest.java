package com.consult.painter.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class PainterTest {

	static final private String ID = "f0e8ce0ac";
	static final private String NAME ="kepa";
	static final private String LASTNAME ="Arrizabalaga";
	static final private String BORNPLACE ="España";
	static final private String MOSTFAMOUSPAINT ="No ha sido un arquero seguro"; 
	
	Painter painter;
	@Test
	void test() throws Exception {
		
		Painter painter = new Painter();
		painter.setId("f0e8ce0ac");
		painter.setLastName( "Arrizabalaga");
		painter.setBornPlace("España");
		painter.setName( "kepa");
		painter.setMostFamousPaint("No ha sido un arquero seguro");
        Assert.assertEquals("ID ok", ID, painter.getId());
        Assert.assertEquals("NAME ok", NAME, painter.getName());
        Assert.assertEquals("LASTNAME ok", LASTNAME, painter.getLastName());
        Assert.assertEquals("BORNPLACE ok", BORNPLACE, painter.getBornPlace());
        Assert.assertEquals("MOSTFAMOUSPAINT ok", MOSTFAMOUSPAINT, painter.getMostFamousPaint());
       System.out.println("fin de la validadción");
	}

}
