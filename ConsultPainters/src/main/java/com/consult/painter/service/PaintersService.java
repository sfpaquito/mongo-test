package com.consult.painter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consult.painter.repository.PaintersRepository;

@Service
public class PaintersService {

	@Autowired
	PaintersRepository reppository;
	
	public List listAllPainters() {
		List listResponse;
		listResponse = reppository.findAllPainters();
		return listResponse;
	}
}
