package com.consult.painter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	private static final String SWAGGER_API_VERSION = "0.0.0";
	private static final String LICENSE_TEXT = "Plantillas sngular";
	private static final String TITLE = "Plantilla JPA";
	private static final String DESCRIPTION = "Plantilla JPA para consumo de una base de datos postgreSQL ";

	@Bean
	public Docket productApi() {

		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.consult.painter.controller")).build().apiInfo(apiInfo())
				.pathMapping("/");

	}

	private ApiInfo apiInfo() {

		return new ApiInfoBuilder().title(TITLE).description(DESCRIPTION).license(LICENSE_TEXT)
				.version(SWAGGER_API_VERSION).build();

	}
}
