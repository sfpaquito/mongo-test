package com.consult.painter.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.consult.painter.dao.PainterDAO;

@Repository
public class PaintersRepository {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	public List<PainterDAO> findAllPainters() {
		return mongoTemplate.findAll(PainterDAO.class);
	}
}
