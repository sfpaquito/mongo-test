package com.consult.painter.dao;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document("painter")
public class PainterDAO {

	@Id
	private String id;
	private String nombre;
	private String apellido;
	@Field("lugar de nacimiento")
	private String lugarDeNacimiento;
	@Field("pintura mas famosa")
	private String pinturaMasFamosa;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getLugarDeNacimiento() {
		return lugarDeNacimiento;
	}

	public void setLugarDeNacimiento(String lugarDeNacimiento) {
		this.lugarDeNacimiento = lugarDeNacimiento;
	}

	public String getPinturaMasFamosa() {
		return pinturaMasFamosa;
	}

	public void setPinturaMasFamosa(String pinturaMasFamosa) {
		this.pinturaMasFamosa = pinturaMasFamosa;
	}

}
