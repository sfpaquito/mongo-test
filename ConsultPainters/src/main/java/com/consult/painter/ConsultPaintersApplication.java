package com.consult.painter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultPaintersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultPaintersApplication.class, args);
	}

}
