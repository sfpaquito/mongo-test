package com.consult.painter.model;

public class Painter {

	private String id;
	private String name;
	private String lastName;
	private String bornPlace;
	private String mostFamousPaint;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBornPlace() {
		return bornPlace;
	}

	public void setBornPlace(String bornPlace) {
		this.bornPlace = bornPlace;
	}

	public String getMostFamousPaint() {
		return mostFamousPaint;
	}

	public void setMostFamousPaint(String mostFamousPaint) {
		this.mostFamousPaint = mostFamousPaint;
	} 
}
