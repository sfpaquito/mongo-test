package com.consult.painter.model;

import java.util.List;

public class PaintersListResponse {

	private List<Painter> painters;
	private String responseMessage;

	public List<Painter> getPainters() {
		return painters;
	}

	public void setPainters(List<Painter> painters) {
		this.painters = painters;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
}
